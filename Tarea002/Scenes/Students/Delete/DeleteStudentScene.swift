//
//  DeleteStudentScene.swift
//  DZIntranet
//
//  Created by Kenyi Rodriguez on 21/04/22.
//

import Foundation

class DeleteStudentScene: Scene {
    
    var inputs = DeleteStudentSceneInputs()
    
    override func drawView() {
        super.drawView()
        self.getSearchKey()
    }
    
    
    private func getSearchKey() {
        let searchKey = self.inputs.txtSearch.getInput()
        guard let student = StudentsDataSource.shared.searchByKey(searchKey) else {
            print("\nNo se encontraron resultado para: \(searchKey)")
            _ = readLine()
            self.getSearchKey()
            return
        }
        self.confirmDeletionOfStudent(student)
        self.backScene()
    }
    private func confirmDeletionOfStudent(_ student:Student){
         print("Esta seguro de eliminar al alumno : \(student.fullName)?")
        let confirmValue = self.inputs.txtConfirmDelete.getInput()
        let intValue = Int(confirmValue) ?? 0
        switch intValue{
          case 1 : StudentsDataSource.shared.deleteStudent(student)
          default :break
        }
    }
   
}


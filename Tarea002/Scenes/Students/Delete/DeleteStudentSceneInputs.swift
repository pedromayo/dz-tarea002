//
//  DeleteStudentSceneInputs.swift
//  Tarea002
//
//  Created by pedro mayo on 24/04/22.
//

import Foundation


struct DeleteStudentSceneInputs {
    
    lazy var txtSearch: Input.Text = {
        let message = """
        Ingrese el dato por cual usted desea eliminar al alumno, puede buscar por:
        (*) - DNI
        (*) - Nombre
        (*) - Apellido  
        """
        return Input.Text(message: message,
                   errorMessage: "El dato de búsqueda es incorrecto",
                   minLength: 1)
    }()
    
    
    lazy var txtConfirmDelete: Input.Integer = {
        let message = """
        Está seguro que desea eliminar al alumno? Presione :
        (1) - Para confirmar
        (0) - Para abortar
        """
        return Input.Integer(message: message, errorMessage: "Debe elegir una de las opciones", range: 0...1)
    }()
    
}
